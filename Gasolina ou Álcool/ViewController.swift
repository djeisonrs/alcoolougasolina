//
//  ViewController.swift
//  Gasolina ou Álcool
//
//  Created by Djeison Aquino on 18/08/17.
//  Copyright © 2017 Djeison Aquino. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    

    @IBOutlet weak var precoAlcoolTextField: UITextField!
    @IBOutlet weak var precoGasolinaTextField: UITextField!
    @IBOutlet weak var resultadoCalculo: UILabel!
    
    @IBAction func botaoCalcular(_ sender: Any) {
        
        var precoAlcool: Double = 0
        var precoGasolina: Double = 0
        var calculoFinal: Double = 0
        
        if let resultadoAlcool = precoAlcoolTextField.text{
            if resultadoAlcool != ""{
                precoAlcool = Double(resultadoAlcool)!
            }
        }
        
        if let resultadoGasolina = precoGasolinaTextField.text{
            if resultadoGasolina != ""{
                precoGasolina = Double(resultadoGasolina)!
            }
        }
        
        calculoFinal = precoAlcool / precoGasolina
        
        if calculoFinal >= 0.7{
            resultadoCalculo.text = "Melhor utilizar Gasolina!!!"
        }else{
            resultadoCalculo.text = "Melhor utilizar Álcool!!!"
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

